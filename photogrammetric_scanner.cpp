#include <AccelStepper.h>
#include "SevSeg.h"

/*CVAR*/
#define   RELEASE   0
#define   DEBUG     !RELEASE

/*PINS*/
#define   INIT_TRIGGER                          0
#define   TURNTABLE_MOTOR_DIR_PIN               3
#define   TURNTABLE_MOTOR_STEP_PIN              5
#define   CAMERA_TRIGGER                        10
#define   ALERT_DEVICE                          13
#define   ANALOGPIN_SHOTS_PER_TURN              0

#define   NUM_OF_DIGITS                         4
#define   DIGIT1                                1
#define   DIGIT2                                2
#define   DIGIT3                                4
#define   DIGIT4                                0

/*SETTINGS*/
#define   STEPS_PER_REV                         200
#define   TURNTABLE_MAXSPEED                    30
#define   TURNTABLE_ACCELERATION                5

/*GLOBAL VARS*/
AccelStepper turntable(AccelStepper::DRIVER, TURNTABLE_MOTOR_STEP_PIN, TURNTABLE_MOTOR_DIR_PIN);
SevSeg sevseg;
long int g_no_shots_per_rotation, g_step_increments_rotation, g_no_shots_per_rotation_counter;

void triggerCamera (void);

void setup() {
  g_no_shots_per_rotation = 0;
  g_no_shots_per_rotation_counter = 0;
  g_step_increments_rotation = 0;
  turntable.setMaxSpeed(TURNTABLE_MAXSPEED);
  turntable.setAcceleration(TURNTABLE_ACCELERATION);
  pinMode(CAMERA_TRIGGER, OUTPUT);
  pinMode(ALERT_DEVICE, OUTPUT);
  pinMode(INIT_TRIGGER, INPUT);

  byte digitPins[] = {DIGIT1, DIGIT2, DIGIT3, DIGIT4};
  //{a, b, c, d, e, f, g, h}
  byte segmentPins[] = {6 , 7, 8, 9, 10, 11, 12, 13};
  sevseg.begin(COMMON_ANODE, NUM_OF_DIGITS, digitPins, segmentPins);

#if DEBUG
  Serial.begin(9600);
  while (!Serial)  ;  //wait for a connection over the serial port
#endif

  turntable.stop();
}

void loop() {
  //read user input
  g_no_shots_per_rotation = (long int)((((analogRead(ANALOGPIN_SHOTS_PER_TURN) * 1024) / 5 ) * 50) / 1024);
  //display equivalent shots per turn in the 7 segment display, max 50 shots per rotation

  sevseg.setNumber(g_no_shots_per_rotation);
  for(unsigned int l_counter = sizeof(l_counter); l_counter != 0; l_counter--)
  {
    sevseg.refreshDisplay();
  }

  if (INIT_TRIGGER)
  {
    g_no_shots_per_rotation_counter = g_no_shots_per_rotation;
    g_step_increments_rotation = (unsigned long int)(STEPS_PER_REV / g_no_shots_per_rotation);

    while (g_no_shots_per_rotation_counter--)
    {
      triggerCamera();
      turntable.moveTo(g_step_increments_rotation);

      while (turntable.distanceToGo() != 0)
      {
        turntable.run();
      }
    }
  }
}

void triggerCamera (void) {
  delay(1000);
  digitalWrite(CAMERA_TRIGGER, HIGH);
  delay(4000);
  digitalWrite(CAMERA_TRIGGER, LOW);
  delay(2000);
}